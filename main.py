#!/bin/python3

import math
import os
import random
import re
import sys

# Complete the jumpingOnClouds function below.
def jumpingOnClouds(c, k):
    e = 100  # initial energy level
    n = len(c)

    place = k % n
    if c[place] == 1:
        e -= 3
    else:
        e -= 1

    while place != 0:
        place = (place + k) % n
        if c[place] == 1:
            e -= 3
        else:
            e -= 1

    return e

if __name__ == '__main__':
    fptr = open(os.environ['OUTPUT_PATH'], 'w')

    nk = input().split()

    n = int(nk[0])

    k = int(nk[1])

    c = list(map(int, input().rstrip().split()))

    result = jumpingOnClouds(c, k)

    fptr.write(str(result) + '\n')

    fptr.close()
