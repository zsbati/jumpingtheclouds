A child is playing a cloud hopping game. In this game, there are sequentially numbered clouds that can be thunderheads or cumulus clouds. The character must jump from cloud to cloud until it reaches the start again.

There is an array of clouds, c and an energy level 100. The character starts from 0 and uses  1 unit of energy to make a jump of size  to cloud (i+k)%n. If it lands on a thundercloud, 1, its energy (e) decreases by 2 additional units. The game ends when the character lands back on cloud .

Given the values of n, and k, and the configuration of the clouds as an array c, determine the final value of  after the game ends.


